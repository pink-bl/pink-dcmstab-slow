#!/usr/bin/python3

import epics
import numpy as np
import time

## create epics channels
print("Creating PV channels...")
enable_pv = epics.PV("PINK:DCM:stab:enable", auto_monitor=True)
source_pv = epics.PV("PINK:DCM:stab:source", auto_monitor=True)
pitch_deadband_pv = epics.PV("PINK:DCM:pitch:deadband", auto_monitor=True)
pitch_step_pv = epics.PV("PINK:DCM:pitch:step", auto_monitor=True)
bpm_deadband_pv = epics.PV("PINK:DCM:bpm:deadband", auto_monitor=True)
bpm_step_pv = epics.PV("PINK:DCM:bpm:step", auto_monitor=True)

pitch_avg_pv = epics.PV("PINK:DCM:pitch:avg", auto_monitor=False)
bpm_yavg_pv = epics.PV("PINK:DCM:bpm:yavg", auto_monitor=False)
pitch_volt_pv = epics.PV("MONOY01U112L:Piezo2U1", auto_monitor=False)
bpm_valid_pv = epics.PV("PINK:BPM:2:valid", auto_monitor=True)

print("... PVs created")

## variables
DEBUG=False
state=0
enable=0
loop_period=2

print("\nDCM stabilization script running...\n")
while(True):
    try:
        enable=int(enable_pv.get(as_string=False))
        if enable==1:
            if state==0:
                source=int(source_pv.get(as_string=False))
                if source==0:
                    # pitch as source
                    state=10
                elif source==1:
                    # bpm2 as source
                    state=20
                else:
                    state=0
            ## grab current value as reference
            elif state==10:
                ref = pitch_avg_pv.get()
                print("[{}] Pitch reference: {:.3f}".format(time.asctime(), ref))
                state=11
            ## pitch stablization loop
            elif state==11:
                sensor=pitch_avg_pv.get()
                deadband=pitch_deadband_pv.value
                step=pitch_step_pv.value
                err=ref-sensor
                A=np.sign(err)
                if (np.abs(err)>deadband):
                    pvolt = pitch_volt_pv.get()
                    outv = pvolt+(A*step)
                    if DEBUG: print("err:{:.3f}, OV: {}".format(err,outv))
                    pitch_volt_pv.put(outv)
            # BPM stab. set reference
            elif state==20:
                ref = bpm_yavg_pv.get()
                print("[{}] BPM2 Y reference: {:.3f}".format(time.asctime(), ref))
                state=21
            ## bpm stablization loop
            elif state==21:
                if bpm_valid_pv.value:
                    sensor=bpm_yavg_pv.get()
                    deadband=bpm_deadband_pv.value
                    step=bpm_step_pv.value
                    err=ref-sensor
                    A=np.sign(err)
                    if (np.abs(err)>deadband):
                        pvolt = pitch_volt_pv.get()
                        outv = pvolt+(A*step)
                        if DEBUG: print("err:{:.3f}, OV: {}".format(err,outv))
                        pitch_volt_pv.put(outv)
        else:
            state=0

    except Exception as err:
        print("[{}] Error:".format(time.asctime()))
        print(err)

    #if DEBUG: print("State: {}".format(state))
    time.sleep(loop_period)

print("OK")
