#!../../bin/linux-x86_64/dcm

## You may have to change dcm to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/dcm.dbd"
dcm_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/dcm.db", "BL=PINK,DEV=DCM")
dbLoadRecords("db/fbk.db", "BL=PINK,DEV=DCM,PITCH_N=20,BPM_N=6")

cd "${TOP}/iocBoot/${IOC}"

set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=PINK,DEV=DCM")

## Start any sequence programs
#seq sncxxx,"user=epics"
