# pink-dcmstab-slow

EPICS IOC + python script for slow DCM stabilization (<1Hz)

## Docker-compose.yml
```yml
version: "3.7"
services:
  ioc:
    image: docker.gitlab.gwdg.de/pink-bl/pink-dcmstab-slow/dcmstab-slow:v1.0
    container_name: dcmstab-slow-ioc
    network_mode: host
    stdin_open: true
    tty: true
    restart: always
    working_dir: "/EPICS/IOCs/pink-dcmstab-slow/iocBoot/iocdcm"
    command: "./dcmstab.cmd"
    volumes:
        - type: bind
          source: /EPICS/autosave/dcmstab-slow
          target: /EPICS/autosave
  py:
    image: docker.gitlab.gwdg.de/pink-bl/pink-dcmstab-slow/dcmstab-slow:v1.0
    container_name: dcmstab-slow-py
    network_mode: host
    stdin_open: true
    tty: true
    restart: always
    depends_on:
        - "ioc"
    working_dir: "/EPICS/IOCs/pink-dcmstab-slow/script"
    command: "./dcmstab.py"
```
